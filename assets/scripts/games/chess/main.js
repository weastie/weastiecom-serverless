/* globals $ Graphics, WebSocket, Chess */

var chess;

let socket = new WebSocket('wss://4ggva96953.execute-api.us-east-1.amazonaws.com/prod');

var curGame = {};
var isWhite; // is the player white or black
var token;

var lastMoveTime = 0;

function sockSend (obj) {
	socket.send(JSON.stringify(obj));
}

socket.onopen = function (e) {
	$('#status').html('');
	$('#game').show();
	console.log('socket open');
	// Check if the user was in a game, allow them to rejoin
	if (window.localStorage.game_active === 'true') {
		$(document).ready(function () {
			$('#rejoinGameBtn').removeClass('hidden');
		});
	}
};

socket.onmessage = function (msg) {
	var mdata = JSON.parse(msg.data);
	console.log(mdata);
	if (mdata.event === 'game_created') {
		// You successfully created a game
		curGame = mdata.game;
		isWhite = false;
		if (mdata.legal_moves.length > 0) {
			isWhite = true;
		}
		$('#enterGameDiv').hide();
		// Token so if the page is refreshed we can rejoin
		window.localStorage.token = isWhite ? curGame['white_token'] : curGame['black_token'];
		token = Number(window.localStorage.token);
		window.localStorage.game_active = true;
		window.localStorage.game_code = curGame.game_id;
		updateLegalMoves(mdata.legal_moves);
		$('#myNameDisplay').html(isWhite ? curGame['white_name'] : curGame['black_name']);
		$('#waitGameDiv').show();
		$('#gameCodeDisplay').html(curGame.game_id.toString(32).toUpperCase());
	} else if (mdata.event === 'opp_disconnect') {
		$('#messageLog').append(`<p>Player ${isWhite ? curGame.black_name : curGame.white_name} disconnected.</p>`);
	} else if (mdata.event === 'opp_join') {
		// An opponent joined a game you created
		if (isWhite) {
			curGame.black_name = mdata.opp_name;
		} else {
			curGame.white_name = mdata.opp_name;
		}
		$('#opponentNameDisplay').html(mdata.opp_name);
		$('#messageLog').append(`<p>Player ${mdata.opp_name} joined the game.</p>`);
		Graphics.drawBoard(chess.board);
		$('#waitGameDiv').hide();
		$('#playGameDiv').show();
	} else if (mdata.event === 'game_join') {
		// You have joined someone's game
		curGame = mdata.game;
		isWhite = mdata.is_white;
		window.localStorage.token = isWhite ? curGame['white_token'] : curGame['black_token'];
		token = Number(window.localStorage.token);
		window.localStorage.game_active = true;
		window.localStorage.game_code = curGame.game_id;
		$('#myNameDisplay').html(isWhite ? curGame['white_name'] : curGame['black_name']);
		$('#opponentNameDisplay').html(isWhite ? curGame['black_name'] : curGame['white_name']);
		$('#messageLog').append(`<p>Joined game. You are ${isWhite ? 'white' : 'black'}</p>`);
		$('#enterGameDiv').hide();
		$('#playGameDiv').show();
		Graphics.drawBoard(chess.board);
		for (let m = 0; m < curGame.moves.length; m++) {
			frontEndMakeMove(curGame.moves[m]);
		}
		updateLegalMoves(mdata.legal_moves);
		updateMoveHistory(mdata);
	} else if (mdata.event === 'join_error') {
		if (mdata.message === 'game_full') {
			$('#messageLog').append(`<p>Game full. You can not join</p>`);
		} else if (mdata.message === 'game_not_found') {
			$('#messageLog').append(`<p>Game not found.</p>`);
		};
	} else if (mdata.event === 'move_error') {
		$('#messageLog').append(`<p>Move error: ${mdata.message}.</p>`);
	} else if (mdata.event === 'chat') {
		$('#messageLog').append(`<p><b>${mdata.from}</b>: ${mdata.message.replaceAll('<', '&lt;').replaceAll('>', '&gt;')}</p>`);
	} else if (mdata.event === 'chat_error') {
		$('#messageLog').append(`<p>Chat error: ${mdata.message}</p>`);
	} else if (mdata.event === 'move') {
		console.log(Date.now() - lastMoveTime);
		curGame.moves.push(mdata.move);
		updateMoveHistory(mdata);
		// Check if the move ends the game
		frontEndMakeMove(mdata.move);
		updateLegalMoves(mdata.legal_moves);
	} else if (mdata.event === 'game_end') {
		$('#messageLog').append(`<p>${mdata.result === 0.5 ? 'Draw' : mdata.result === 1 ? 'White wins' : 'Black wins'}.</p>`);
	}
};

function frontEndMakeMove (move) {
	if (move.substr(-1) === '+') {
		chess.makeMove(move.slice(0, -1), this.board);
		$('#messageLog').append(`<p>Check.</p>`);
	} else if (move.substr(-1) === '#') {
		chess.makeMove(move.slice(0, -1), this.board);
		$('#messageLog').append(`<p>Checkmate.</p>`);
		window.localStorage.game_active = false;
	} else if (move.substr(-1) === '=') {
		chess.makeMove(move.slice(0, -1));
		$('#messageLog').append(`<p>Draw.</p>`);
		window.localStorage.game_active = false;
	} else {
		chess.makeMove(move);
	}
	Graphics.drawBoard(chess.board);
}

function createGame () {
	sockSend({
		'action': 'create_game',
		'nickname': $('#nicknameInput').val()
	});
}

function joinGame () {
	sockSend({
		'action': 'join_game',
		'game_id': Number.parseInt($('#joinGameCode').val(), 32),
		'nickname': $('#nicknameInput').val()
	});
}

function rejoinGame () {
	sockSend({
		'action': 'join_game',
		'game_id': window.localStorage.game_code,
		'token': Number(window.localStorage.token)
	});
}

function customMove () {
	let move = $('#moveTextInput').val();
	$('#moveTextInput').removeClass('formError');
	if (curGame.legal_moves.indexOf(move) >= 0) {
		makeMove(move);
		$('#moveTextInput').val('');
	} else {
		$('#moveTextInput').addClass('formError');
	}
}

function makeMove (move) {
	lastMoveTime = Date.now();
	sockSend({
		'action': 'make_move',
		'game_id': curGame.game_id,
		'token': token,
		'move': move
	});
}

function sendChat () {
	sockSend({
		'action': 'send_chat',
		'game_id': curGame.game_id,
		'token': token,
		'message': $('#chatInput').val()
	});
	$('#chatInput').val('');
}

function updateMoveHistory () {
	let html = '';
	for (let i = 0; i < curGame.moves.length; i += 2) {
		html += `<span class='move_item'>${1 + i / 2}. ${curGame.moves[i]}`;
		if ((i + 1) < curGame.moves.length) {
			html += ` ${curGame.moves[i + 1]} `;
		}
		html += `</span>`;
	}
	$('#movesLog').html(html);
}

function updateLegalMoves (legalMoves) {
	curGame.legal_moves = legalMoves;
	$('#legal_moves').html('');
	let lastType = '';
	if (legalMoves && legalMoves.length > 0) {
		for (let i = 0; i < legalMoves.length; i++) {
			if ((legalMoves[i][0] === legalMoves[i][0].toUpperCase() && legalMoves[i][0] !== lastType)) {
				$('#legal_moves').append('<br>');
				lastType = legalMoves[i][0].toUpperCase();
			} else if (legalMoves[i][0] !== legalMoves[i][0].toUpperCase() && lastType !== 'p') {
				$('#legal_moves').append('<br>');
				lastType = 'p';
			}
			$('#legal_moves').append(`<button class='legal_move'>${legalMoves[i]}</button>`);
		}
	}
	$('.legal_move').click(function (e) {
		makeMove(e.currentTarget.innerHTML);
	});
	$('.legal_move').mouseenter(function (e) {
		Graphics.drawBoard(chess.previewMove(e.currentTarget.innerHTML));
		Graphics.preview();
	});
	$('.legal_move').mouseleave(function (e) {
		Graphics.drawBoard(chess.board);
	});
}

$(document).ready(function () {
	Graphics.loadCanvas($('#canvas')[0]);
	chess = new Chess();
	Graphics.loadImages(function () {
		Graphics.drawBoard(chess.board);
	});

	$('#createGameBtn').click(function () { createGame(); });
	$('#joinGameBtn').click(function () { joinGame(); });
	$('#chatButton').click(function () { sendChat(); });
	$('#nicknameInput').val('Anonymous' + Math.floor(1000 + Math.random() * 8999));
	$('#rejoinGameBtn').click(function () { rejoinGame(); });
	$('#moveTextButton').click(function () { customMove(); });
});
