/* globals Image, isWhite */

var Graphics = { // eslint-disable-line
	canvas: null,
	ctx: null,
	cfg: {
		square_size: 64,
		theme: {
			dark: '#8a4c04',
			light: '#c6700e'
		}
	},
	image_folder: 'https://assets.weastie.com/img/games/chess/',
	image_links: {
		bishop_white: 'WHITE_BISHOP.png',
		bishop_black: 'BLACK_BISHOP.png',
		pawn_white: 'WHITE_PAWN.png',
		pawn_black: 'BLACK_PAWN.png',
		knight_white: 'WHITE_KNIGHT.png',
		knight_black: 'BLACK_KNIGHT.png',
		rook_white: 'WHITE_ROOK.png',
		rook_black: 'BLACK_ROOK.png',
		king_white: 'WHITE_KING.png',
		king_black: 'BLACK_KING.png',
		queen_white: 'WHITE_QUEEN.png',
		queen_black: 'BLACK_QUEEN.png'
	},
	images: {},
	loadCanvas: function (canvas) {
		this.canvas = canvas;
		this.canvas.width = this.cfg.square_size * 8;
		this.canvas.height = this.cfg.square_size * 8;
		this.ctx = canvas.getContext('2d');
		document.getElementById('sidePanel').style = 'height: ' + this.canvas.height + 'px; width: ' + this.canvas.height * 0.5 + 'px';
		document.getElementById('sidePanelBody').style = 'height: ' + Math.floor(this.canvas.height * 0.8) + 'px;';
		document.getElementById('sidePanelInput').style = 'height: ' + Math.floor(this.canvas.height * 0.2) + 'px;';
	},
	loadImages: function (callback) {
		let imgCount = 0;
		let loadCount = 0;
		for (var image in this.image_links) {
			imgCount += 1;
			this.images[image] = new Image();
			this.images[image].onload = function () {
				loadCount += 1;
				if (imgCount === loadCount) callback();
			};
			this.images[image].src = this.image_folder + this.image_links[image];
		}
	},
	drawBoard: function (board) {
		// Draw pieces
		for (let i = 0; i < 8; i++) {
			for (let j = 0; j < 8; j++) {
				let tj = j;
				let ti = 7 - i;
				if (isWhite) {
					tj = 7 - j;
					ti = i;
				}
				this.ctx.fillStyle = ((ti + tj) % 2 === 0 ? this.cfg.theme.dark : this.cfg.theme.light);
				this.ctx.fillRect(ti * this.cfg.square_size, tj * this.cfg.square_size, this.cfg.square_size, this.cfg.square_size);
				if (board !== null && board[j][i] !== false) {
					this.drawPiece(this.images[board[j][i].toTitle()], ti, tj);
				}
			}
		}
		// Draw board indicators
		let letters = 'abcdefgh';
		for (let k = 0; k < 8; k++) {
			this.ctx.fillStyle = 'white';
			this.ctx.font = '12px Arial';
			this.ctx.fillText(isWhite ? letters[k] : letters[7 - k], this.cfg.square_size * k + 2, this.cfg.square_size * 8 - 2);
			this.ctx.fillText('' + (isWhite ? (8 - k) : (k + 1)), this.cfg.square_size * 8 - 8, this.cfg.square_size * k + 12);
		}
	},
	drawPiece: function (image, file, rank) {
		this.ctx.drawImage(image, file * this.cfg.square_size, rank * this.cfg.square_size, this.cfg.square_size, this.cfg.square_size);
	},
	preview: function () {
		this.ctx.fillStyle = 'white';
		this.ctx.globalAlpha = 0.2;
		this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
		this.ctx.globalAlpha = 1;
	}
};
