const COLOR_WHITE = 0;
const COLOR_BLACK = 1;

const PIECE_PAWN = 0;
const PIECE_KNIGHT = 1;
const PIECE_BISHOP = 2;
const PIECE_ROOK = 3;
const PIECE_QUEEN = 4;
const PIECE_KING = 5;

const PIECE_TITLES = ['pawn', 'knight', 'bishop', 'rook', 'queen', 'king'];

const DIAG_MOVES = [[1, 1], [1, -1], [-1, -1], [-1, 1]];
const LINE_MOVES = [[1, 0], [-1, 0], [0, 1], [0, -1]];
const KNIGHT_MOVES = [[2, 1], [1, 2], [-1, 2], [-2, 1], [-1, -2], [-2, -1], [1, -2], [2, -1]];

function Piece (piece, color) {
	this.piece = piece;
	this.color = color;
}

// File to index
function f2i (file) {
	return 'abcdefgh'.indexOf(file);
}
// Rank to index
function r2i (n) {
	return n - 1;
}

function codeToPiece (code) {
	return ['', 'N', 'B', 'R', 'Q', 'K'].indexOf(code);
}

// Bounds checking
function bounds (ranki, file) {
	return ranki >= r2i(1) && ranki <= r2i(8) && file >= f2i('a') && file <= f2i('h');
}

Piece.prototype.toTitle = function () {
	return PIECE_TITLES[this.piece] + (this.color === COLOR_WHITE ? '_white' : '_black');
};

function pieceEquals (p1, p2) {
	if (p1 === false || p2 === false) {
		return false;
	} else {
		return p1.piece === p2.piece && p1.color === p2.color;
	}
};

class Chess { // eslint-disable-line
	constructor () {
		this.board = [];
		this.turn = COLOR_WHITE;
		// Populate board
		// Empty
		for (var i = 0; i < 8; i++) {
			this.board[i] = [];
			for (var j = 0; j < 8; j++) {
				this.board[i][j] = false;
			}
		}
		// White
		this.board[0] = [
			new Piece(PIECE_ROOK, COLOR_WHITE),
			new Piece(PIECE_KNIGHT, COLOR_WHITE),
			new Piece(PIECE_BISHOP, COLOR_WHITE),
			new Piece(PIECE_QUEEN, COLOR_WHITE),
			new Piece(PIECE_KING, COLOR_WHITE),
			new Piece(PIECE_BISHOP, COLOR_WHITE),
			new Piece(PIECE_KNIGHT, COLOR_WHITE),
			new Piece(PIECE_ROOK, COLOR_WHITE)
		];
		// Pawns
		for (var k = 0; k < 8; k++) {
			this.board[1][k] = new Piece(PIECE_PAWN, COLOR_WHITE);
			this.board[6][k] = new Piece(PIECE_PAWN, COLOR_BLACK);
		}
		// Black
		this.board[7] = [
			new Piece(PIECE_ROOK, COLOR_BLACK),
			new Piece(PIECE_KNIGHT, COLOR_BLACK),
			new Piece(PIECE_BISHOP, COLOR_BLACK),
			new Piece(PIECE_QUEEN, COLOR_BLACK),
			new Piece(PIECE_KING, COLOR_BLACK),
			new Piece(PIECE_BISHOP, COLOR_BLACK),
			new Piece(PIECE_KNIGHT, COLOR_BLACK),
			new Piece(PIECE_ROOK, COLOR_BLACK)
		];
	}

	previewMove (move) {
		// Copy board
		let newBoard = [];
		for (var i = 0; i < 8; i++) {
			let row = [];
			for (var j = 0; j < 8; j++) {
				if (this.board[i][j] === false) {
					row.push(false);
				} else {
					row.push(new Piece(this.board[i][j].piece, this.board[i][j].color));
				}
			}
			newBoard.push(row);
		}
		this.makeMoveInt(move, newBoard);
		return newBoard;
	}

	makeMove (move) {
		this.makeMoveInt(move, this.board);
		this.turn = (this.turn + 1) % 2;
	}
	// Does not check if move is legal, just does it
	makeMoveInt (move, board) {
		if (/^[a-h][1-8](=[RNBQ])?$/.test(move)) {
			// Pawn move
			let file = f2i(move[0]);
			let ranki = r2i(Number(move[1]));
			let endPiece = PIECE_PAWN;

			if (this.turn === COLOR_WHITE) {
				// Promotion
				if (ranki === r2i(8)) {
					endPiece = codeToPiece(move[3]);
				}

				// Special case with white's 4th rank
				if (ranki === r2i(4) && !pieceEquals(board[r2i(3)][file], new Piece(PIECE_PAWN, COLOR_WHITE))) {
					board[r2i(2)][file] = false;
					board[r2i(4)][file] = new Piece(PIECE_PAWN, COLOR_WHITE);
					return true;
				}

				// Default case for white
				board[ranki - 1][file] = false;
				board[ranki][file] = new Piece(endPiece, COLOR_WHITE);
			} else {
				// Promotion
				if (ranki === r2i(1)) {
					endPiece = codeToPiece(move[3]);
				}

				// Special case with black's 5th rank
				if (ranki === r2i(5) && !pieceEquals(board[r2i(6)][file], new Piece(PIECE_PAWN, COLOR_BLACK))) {
					board[r2i(7)][file] = false;
					board[r2i(5)][file] = new Piece(PIECE_PAWN, COLOR_BLACK);
					return true;
				}

				// Default case for black
				board[ranki + 1][file] = false;
				board[ranki][file] = new Piece(endPiece, COLOR_BLACK);
				return true;
			}
		} else if (/^[a-h]x[a-h][1-8](=[RNBQ])?$/.test(move)) {
      // Pawn capture
			let initFile = f2i(move[0]);
			let endFile = f2i(move[2]);
			let endRanki = r2i(Number(move[3]));
			let endPiece = PIECE_PAWN;

			if (this.turn === COLOR_WHITE) {
				if (endRanki === r2i(6) && !board[endRanki][endFile]) {
					board[endRanki - 1][endFile] = false;
				}
				if (endRanki === r2i(8)) {
					endPiece = codeToPiece(move[5]);
				}
				board[endRanki - 1][initFile] = false;
			} else {
				if (endRanki === r2i(3) && !board[endRanki][endFile]) {
					board[endRanki + 1][endFile] = false;
				}
				if (endRanki === r2i(1)) {
					endPiece = codeToPiece(move[5]);
				}
				board[endRanki + 1][initFile] = false;
			}
			board[endRanki][endFile] = new Piece(endPiece, this.turn);
			return true;
		} else if (/^N([a-h])?([1-8])?(x)?[a-h][1-8]$/.test(move)) {
			// Knight move or capture (they are kinda the same)
			let endFile = f2i(move[move.length - 2]);
			let endRanki = r2i(Number(move[move.length - 1]));
			let initFile = -1;
			let initRanki = -1;

			let moveWithoutCapture = move.replaceAll('x', '');
			// Either init rank or init file was specified
			if (moveWithoutCapture.length === 4) {
				if (moveWithoutCapture[1] >= 'a' && moveWithoutCapture[1] <= 'h') {
					initFile = f2i(move[1]);
				} else {
					initRanki = r2i(Number(move[1]));
				}
			}

			// Init rank and init file were specified
			if (moveWithoutCapture.length === 5) {
				initFile = f2i(move[1]);
				initRanki = r2i(Number(move[2]));
			}

			// If not specified what file or rank, we must find them
			if (initFile === -1 || initRanki === -1) {
				for (let k = 0; k < KNIGHT_MOVES.length; k++) {
					let kMove = KNIGHT_MOVES[k];
					if (bounds(endRanki + kMove[0], endFile + kMove[1])) {
						// Basically, if initFile or initRank are specified, must match them
						if ((initFile === -1 || initFile === endFile + kMove[1]) && (initRanki === -1 || initRanki === endRanki + kMove[0])) {
							if (pieceEquals(board[endRanki + kMove[0]][endFile + kMove[1]], new Piece(PIECE_KNIGHT, this.turn))) {
								initFile = endFile + kMove[1];
								initRanki = endRanki + kMove[0];
							}
						}
					}
				}
			}
			// Bad move
			if (initFile === -1 || initRanki === -1) {
				console.log('bad move');
				return false;
			}
			// Put knight at the end position
			board[endRanki][endFile] = new Piece(PIECE_KNIGHT, this.turn);
			// Clear the init position
			board[initRanki][initFile] = false;
			return true;
		} else if (/^([BQR])([a-h])?([1-8])?(x)?[a-h][1-8]$/.test(move)) {
			let endFile = f2i(move[move.length - 2]);
			let endRanki = r2i(Number(move[move.length - 1]));
			let initFile = -1;
			let initRanki = -1;

			let moveWithoutCapture = move.replaceAll('x', '');
			// Either init rank or init file was specified
			if (moveWithoutCapture.length === 4) {
				if (moveWithoutCapture[1] >= 'a' && moveWithoutCapture[1] <= 'h') {
					initFile = f2i(move[1]);
				} else {
					initRanki = r2i(Number(move[1]));
				}
			}

			// Init rank and init file were specified
			if (moveWithoutCapture.length === 5) {
				initFile = f2i(move[1]);
				initRanki = r2i(Number(move[2]));
			}

			let directions = [];
			let pIndex = -1;
			if (move[0] === 'B' || move[0] === 'Q') {
				directions = directions.concat(DIAG_MOVES);
				pIndex = PIECE_BISHOP;
			}
			if (move[0] === 'R' || move[0] === 'Q') {
				directions = directions.concat(LINE_MOVES);
				pIndex = (pIndex === PIECE_BISHOP ? PIECE_QUEEN : PIECE_ROOK);
			}

			// If not specified what file or rank, we must find it by searching from the end position
			for (let d = 0; d < directions.length; d++) {
				let dr = directions[d][0]; // delta rank
				let df = directions[d][1]; // delta file
				let m = 1;
				while (bounds(endRanki + dr * m, endFile + df * m)) {
					if ((initFile === -1 || initFile === endFile + df * m) && (initRanki === -1 || initRanki === endRanki + dr * m)) {
						if (board[endRanki + dr * m][endFile + df * m] !== false) {
							if (pieceEquals(board[endRanki + dr * m][endFile + df * m], new Piece(pIndex, this.turn))) {
								initFile = endFile + df * m;
								initRanki = endRanki + dr * m;
							}
							break;
						}
					}
					m += 1;
				}
			}
			if (initFile === -1 || initRanki === -1) {
				console.log('bad move');
				return false;
			}

			board[endRanki][endFile] = new Piece(pIndex, this.turn);
			board[initRanki][initFile] = false;
			return true;
		} else if (/^K(x)?[a-h][1-8]$/.test(move)) {
			let endFile = f2i(move[move.length - 2]);
			let endRanki = r2i(Number(move[move.length - 1]));
			let initFile = -1;
			let initRanki = -1;

			let directions = DIAG_MOVES.concat(LINE_MOVES);
			for (let d = 0; d < directions.length; d++) {
				let dr = directions[d][0]; // delta rank
				let df = directions[d][1]; // delta file

				if (bounds(endRanki + dr, endFile + df)) {
					if (pieceEquals(board[endRanki + dr][endFile + df], new Piece(PIECE_KING, this.turn))) {
						initFile = endFile + df;
						initRanki = endRanki + dr;
						break;
					}
				}
			}

			if (initFile === -1 || initRanki === -1) {
				console.log('bad move');
				return false;
			}

			board[endRanki][endFile] = new Piece(PIECE_KING, this.turn);
			board[initRanki][initFile] = false;
			return true;
		} else if (move === 'O-O') {
			let cRanki = this.turn * 7;

			board[cRanki][f2i('e')] = false;
			board[cRanki][f2i('g')] = new Piece(PIECE_KING, this.turn);

			board[cRanki][f2i('h')] = false;
			board[cRanki][f2i('f')] = new Piece(PIECE_ROOK, this.turn);
			return true;
		} else if (move === 'O-O-O') {
			let cRanki = this.turn * 7;

			board[cRanki][f2i('e')] = false;
			board[cRanki][f2i('c')] = new Piece(PIECE_KING, this.turn);

			board[cRanki][f2i('a')] = false;
			board[cRanki][f2i('d')] = new Piece(PIECE_ROOK, this.turn);
			return true;
		}
	}
}
