/* globals $ */

class Instrument {
	constructor (tunings, frets, markings) {
		this.numStrings = tunings.length;
		this.tunings = tunings;
		this.frets = frets;
		this.markings = markings;
	}
}

var notes = {
	0: ['A'],
	1: ['A#', 'Bb'],
	2: ['B', 'Cb'],
	3: ['C', 'B#'],
	4: ['C#', 'Db'],
	5: ['D'],
	6: ['D#', 'Eb'],
	7: ['E', 'Fb'],
	8: ['F', 'E#'],
	9: ['F#', 'Gb'],
	10: ['G'],
	11: ['G#', 'Ab']
};

var scales = {
	major: [2, 2, 1, 2, 2, 2, 1],
	minor: [2, 1, 2, 2, 1, 2, 2],
	minor_melodic: [2, 1, 2, 2, 2, 2, 1],
	minor_harmonic: [2, 1, 2, 2, 1, 3, 1],
	ionian: [2, 2, 1, 2, 2, 2, 1],
	dorian: [2, 1, 2, 2, 2, 1, 2],
	phrygian: [1, 2, 2, 2, 1, 2, 2],
	lydian: [2, 2, 2, 1, 2, 2, 1],
	mixolydian: [2, 2, 1, 2, 2, 1, 2],
	aeolian: [2, 1, 2, 2, 1, 2, 2],
	locrian: [1, 2, 2, 1, 2, 2, 2]
};

function getNote (string, fret) {
	// find starting note
	var stringNote = -1;
	for (var i = 0; i < 12; i++) {
		if (notes[i].indexOf(curInstrument.tunings[string]) > -1) {
			stringNote = i;
			break;
		}
	}
	return (stringNote + fret) % 12;
}

function getNoteInd (name) {
	for (var i = 0; i < 12; i++) {
		if (notes[i].indexOf(name) > -1) {
			return i;
		}
	}
}

function getNoteName (index, search) {
	for (var i = 0; i < notes[index].length; i++) {
		if (notes[index][i].indexOf(search) >= 0) {
			return notes[index][i];
		}
	}
	return notes[index][0];
}

var instrumentPresets = {
	'bass': new Instrument(['E', 'A', 'D', 'G'], 20, [3, 5, 7, 9, 12, 15, 17, 19]),
	'guitar': new Instrument(['E', 'A', 'D', 'G', 'B', 'E'], 22, [3, 5, 7, 9, 12, 15, 17, 19, 21]),
	'ukelele': new Instrument(['G', 'C', 'E', 'A'], 15, [5, 7, 10, 12, 14])
};

var curInstrument;
var canvas, ctx;
var curHoverNote;
var noteBoxes = [];
var discovery = {
	enabled: false,
	root: 3,
	notes: [3, 5, 7, 8, 10, 0, 2],
	intervals: {
		'1': true,
		'2': true,
		'3': true,
		'4': true,
		'5': true,
		'6': true,
		'7': true
	}
};
var quiz = {
	enabled: false,
	types: {
		note: true,
		key: true
	},
	score: 0,
	numQuestions: 0,
	incorrect: 0,
	note: null,
	answer: ''
};

var natNotes = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];

$(document).ready(function () {
	canvas = document.getElementById('canvas');
	canvas.width = '800';
	canvas.height = '400';
	if (window.innerWidth > 1300) {
		canvas.width = '1000';
		$('#panel').css('width', '1000px');
		$('.panelOptions ul').css('width', '1005px');
	}
	window.onresize = function () {
		canvas.width = '800';
		canvas.height = '400';
		if (window.innerWidth > 1300) {
			canvas.width = '1000';
			$('#panel').css('width', '1000px');
			$('.panelOptions ul').css('width', '1005px');
		}
		drawFretboard();
	};
	ctx = canvas.getContext('2d');
	changeInstrument(instrumentPresets.bass);
	$(canvas).mousemove(function (e) {
		var mx = e.pageX - $(canvas).offset().left;
		var my = e.pageY - $(canvas).offset().top;
		if (curHoverNote) {
			if (distance(curHoverNote.x, curHoverNote.y, mx, my) > curHoverNote.radius) {
				curHoverNote = null;
				drawFretboard();
			}
		} else {
			for (var i = 0; i < noteBoxes.length; i++) {
				var n = noteBoxes[i];
				if (distance(n.x, n.y, mx, my) < n.radius) {
					curHoverNote = n;
					drawFretboard();
					break;
				}
			}
		}
	});
	$(canvas).click(function (e) {
		var mx = e.pageX - $(canvas).offset().left;
		var my = e.pageY - $(canvas).offset().top;
		for (var i = 0; i < noteBoxes.length; i++) {
			var n = noteBoxes[i];
			var d = distance(n.x, n.y, mx, my);
			if (d < n.radius && !n.active2) {
				n.active1 = !n.active1;
			}
		}
		drawFretboard();
	});

	// UI interaction
	$('.panel-button').click(function (e) {
		e.preventDefault();
		$('.panel-button').removeClass('selected');
		$(e.target).addClass('selected');
		$('.panel-page').addClass('hidden');
		if (e.target.getAttribute('for') !== 'panel-quiz') {
			if (quiz.enabled) {
				quiz.enabled = false;
				$('#quizOptions').show();
				$('#quizNotePanel').addClass('hidden');
				$('#quizKeyPanel').addClass('hidden');
				$('#panel-results').addClass('hidden');
				drawFretboard();
			}
		}
		// $(e.target)
		$('#' + e.target.getAttribute('for')).removeClass('hidden');
	});
	$('#instruPreset').change(function (e) {
		var newIns = instrumentPresets[$('#instruPreset').val()];
		changeInstrument(newIns);
	});
	$('#instruFrets').change(function (e) {
		var newFrets = $('#instruFrets').val();
		if (!isNaN(newFrets) && newFrets.length > 0) {
			$('#instruFrets').removeClass('formError');
			curInstrument = new Instrument(curInstrument.tunings, Number.parseInt(newFrets), curInstrument.markings);
			changeInstrument(curInstrument);
		} else {
			$('#instruFrets').addClass('formError');
		}
	});
	$('#instruStrings').change(function (e) {
		var newStrings = $('#instruStrings').val();
		if (!isNaN(newStrings) && newStrings.length > 0) {
			newStrings = Number.parseInt(newStrings);
			$('#instruStrings').removeClass('formError');
			var newTunings = curInstrument.tunings.slice(); // deep copy
			if (curInstrument.tunings.length > newStrings) {
				// There are too many tunings now
				newTunings = newTunings.splice(0, newStrings);
			} else if (curInstrument.tunings.length < newStrings) {
				// There are too little tunings, let's make the new ones 'A' temporarily
				for (var i = 0; i < newStrings - curInstrument.tunings.length; i++) {
					newTunings.push('A');
				}
			}
			curInstrument = new Instrument(newTunings, curInstrument.frets, curInstrument.markings);
			changeInstrument(curInstrument);
		} else {
			$('#instruStrings').addClass('formError');
		}
	});
	$('#instruTunings').change(function (e) {
		var newTunings = $('#instruTunings').val();
		var regex = /^([A-G][b#]?(,))+([A-G][b#]?)$/;
		if (regex.test(newTunings)) {
			$('#instruTunings').removeClass('formError');
			var strings = newTunings.split(',');
			$('#instruStrings').val(strings.length);
			curInstrument = new Instrument(strings, curInstrument.frets, curInstrument.markings);
			changeInstrument(curInstrument);
		} else {
			$('#instruTunings').addClass('formError');
		}
	});
	$('#instruMarkings').change(function (e) {
		var newMarkings = $('#instruMarkings').val();
		var regex = /^([0-9]+(,))+([0-9]+)$/;
		if (regex.test(newMarkings)) {
			$('#instruMarkings').removeClass('formError');
			newMarkings = newMarkings.split(',').map(function (item) {
				return Number.parseInt(item, 10);
			});
			curInstrument = new Instrument(curInstrument.tunings, curInstrument.frets, newMarkings);
			changeInstrument(curInstrument);
		} else {
			$('#instruMarkings').addClass('formError');
		}
	});
	// Discovery
	discovery.enabled = $('#discoEnable').is(':checked');
	// Get intervals
	for (var i = 1; i <= 7; i++) {
		discovery.intervals[i] = $('#scaleInt' + i).is(':checked');
	}
	updateDiscovery();
	$('#discoEnable').change(function (e) {
		discovery.enabled = $('#discoEnable').is(':checked');
		updateDiscovery();
	});
	$('#discoRoot').change(function (e) {
		updateDiscovery();
	});
	$('#discoScale').change(function (e) {
		updateDiscovery();
	});
	$('.scaleInt').change(function (e) {
		discovery.intervals[e.target.id.substr(-1)] = $(e.target).is(':checked');
		updateDiscovery();
	});
	// Quiz
	$('#quizStart').click(function () {
		quiz.enabled = true;
		quiz.types.note = $('#quizNote').is(':checked');
		quiz.types.key = $('#quizKey').is(':checked');
		quiz.score = 0;
		quiz.numQuestions = 0;
		quiz.incorrect = 0;
		$('#results').html('');
		$('#results-score').html('Score: 0/0');
		$('#panel-results').removeClass('hidden');
		$('#quizOptions').hide();
		createQuizQuestion();
	});
	$('.quizNoteBtn').click(function (e) {
		answerQuizQuestion(getNoteInd(e.target.value));
	});
	$('.quizKeyBtn').click(function (e) {
		answerQuizQuestion(e.target.value);
	});
	drawFretboard();
});

function distance (x1, y1, x2, y2) {
	return Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
}

function generateNoteBoxes () {
	noteBoxes = [];
	for (var i = 0; i < curInstrument.numStrings; i++) {
		for (var j = 1; j <= curInstrument.frets; j++) {
			var note = getNote(curInstrument.numStrings - i - 1, j);
			var string = curInstrument.tunings[curInstrument.tunings.length - i - 1];
			var noteName = string.indexOf('b') >= 0 ? getNoteName(note.note, 'b') : notes[note][0];
			var active2 = false;
			var active3 = false;
			var innerText = '';
			if (discovery.enabled) {
				if (discovery.notes.indexOf(note) > -1) {
					if (discovery.intervals[discovery.notes.indexOf(note) + 1]) {
						if (discovery.notes.indexOf(note) === 0) {
							active2 = true;
						} else {
							active3 = true;
						}
						innerText = discovery.notes.indexOf(note) + 1 + '';
						noteName = getNoteName(note, natNotes[(natNotes.indexOf(discovery.root[0]) + discovery.notes.indexOf(note)) % 7]);
					}
				}
			}
			noteBoxes.push({
				x: getFretWidth(j) - getFretWidthInt() / 2,
				y: getStringHeight(i),
				radius: 13,
				active1: false,
				active2: active2,
				active3: active3,
				string: string,
				fret: j,
				innerText: innerText,
				note: note,
				noteName: noteName
			});
		}
	}
}

function updateDiscovery () {
	var rootCheck = /^([A-G][b#]?)$/;
	if (rootCheck.test($('#discoRoot').val())) {
		$('#discoRoot').removeClass('formError');
		discovery.root = $('#discoRoot').val();
		discovery.notes = [];
		var rootInd = getNoteInd(discovery.root);
		var scale = scales[$('#discoScale').val()];
		var sum = 0;
		for (var i = 0; i < scale.length; i++) {
			discovery.notes.push((rootInd + sum) % 12);
			sum += scale[i];
		}
	} else {
		$('#discoRoot').addClass('formError');
	}
	generateNoteBoxes();
	drawFretboard();
}

function createQuizQuestion () {
	// Choose a note - random string random fret
	$('#quizNotePanel').addClass('hidden');
	$('#quizKeyPanel').addClass('hidden');
	var string = curInstrument.tunings[Math.floor(Math.random() * curInstrument.numStrings)];
	var fret = Math.floor(Math.random() * curInstrument.frets) + 1;
	for (var i = 0; i < noteBoxes.length; i++) {
		if (noteBoxes[i].string === string && noteBoxes[i].fret === fret) {
			quiz.note = noteBoxes[i];
		}
	}
	var mode = 0;
	if (quiz.types.note && quiz.types.key) {
		mode = Math.floor(Math.random() * 2);
	} else if (quiz.types.key) {
		mode = 1;
	} else {
		mode = 0;
	}
	if (mode === 0) {
		// Guess the note
		quiz.answer = getNote(curInstrument.tunings.indexOf(string), fret);
		$('#quizNotePanel').removeClass('hidden');
		$('#quizKeyPanel').addClass('hidden');
	} else if (mode === 1) {
		// Note in key?
		var root = Math.floor(Math.random() * 12);
		var scale = 'minor';
		if (Math.floor(Math.random() * 2)) {
			scale = 'major';
		}
		var note = getNote(curInstrument.tunings.indexOf(string), fret);
		quiz.answer = 'No';
		var notesInScale = [root];
		for (var j = 0; j < scales[scale].length; j++) {
			notesInScale.push((notesInScale[notesInScale.length - 1] + scales[scale][j]) % 12);
		}
		console.log(notesInScale);
		if (notesInScale.indexOf(note) > -1) {
			quiz.answer = 'Yes';
		}
		$('#quizKeyDesc').html(notes[root][Math.floor(Math.random()) * notes[root].length] + ' ' + scale);
		$('#quizNotePanel').addClass('hidden');
		$('#quizKeyPanel').removeClass('hidden');
	}
	drawFretboard();
}

function answerQuizQuestion (answer) {
	if (answer === quiz.answer) {
		$('#results').css('color', 'darkgreen');
		$('#results').html('Correct!');
		quiz.score += 1;
		quiz.numQuestions += 1;
		$('#results-score').html('Score: ' + quiz.score + '/' + (quiz.numQuestions + quiz.incorrect));
		createQuizQuestion();
	} else {
		$('#results').css('color', 'darkred');
		$('#results').html('Incorrect');
		quiz.incorrect += 1;
		$('#results-score').html('Score: ' + quiz.score + '/' + (quiz.numQuestions + quiz.incorrect));
	}
}

function changeInstrument (instrument) {
	curInstrument = instrument;
	$('#instruFrets').val(curInstrument.frets);
	$('#instruStrings').val(curInstrument.numStrings);
	$('#instruTunings').val(curInstrument.tunings.join(','));
	$('#instruMarkings').val(curInstrument.markings.join(','));
	generateNoteBoxes();
	drawFretboard();
}

// 0 based, returns graphical height of a string
function getStringHeight (i) {
	return Math.round((canvas.height - 30) * ((i + 1) / (curInstrument.numStrings + 1)));
}

// Returns height interval (spacing) between consecutive strings
function getStringHeightInt () {
	return Math.round((canvas.height - 30) * (1 / (curInstrument.numStrings + 1)));
}

// 1 based, returns graphical width (x) of a fret position
function getFretWidth (j) {
	return 60 + Math.round((j / curInstrument.frets) * (canvas.width - 90));
}

// Return width interval (spacing) between consecutive frets
function getFretWidthInt () {
	return Math.round((1 / curInstrument.frets) * (canvas.width - 90));
}

function drawNote (note) {
	ctx.font = 'bold 17pt Arial';
	ctx.fillStyle = 'green';
	if (note.active1 || quiz.enabled) {
		ctx.fillStyle = 'darkgreen';
	} else if (note.active2) {
		ctx.fillStyle = 'red';
	} else if (note.active3) {
		ctx.fillStyle = 'blue';
	}
	ctx.beginPath();
	ctx.arc(note.x, note.y, note.radius, 0, 2 * Math.PI, false);
	ctx.fill();
	ctx.fillStyle = 'white';
	if (!quiz.enabled) {
		// Append a flat if the string is flat
		ctx.fillText(note.noteName, note.noteName.length === 1 ? note.x - 8 : note.x - 14, note.y - 16);
		// Check for inner text
		if (note.innerText.length > 0) {
			// ctx.fillStyle = 'black';
			ctx.fillText(note.innerText, note.x - 6, note.y + 7);
		}
	}
}

function drawFretboard () {
	// Clear board
	ctx.fillStyle = '#cca97b';
	ctx.font = '20pt Arial';
	ctx.fillRect(0, 0, canvas.width, canvas.height);
	ctx.fillStyle = 'black';
	// Draw strings
	for (var i = 0; i < curInstrument.numStrings; i++) {
		// Write letter of string tuning
		var height = getStringHeight(i);
		var stringName = curInstrument.tunings[curInstrument.numStrings - i - 1];
		if (discovery.enabled && !quiz.enabled) {
			var ind = discovery.notes.indexOf(getNote(curInstrument.numStrings - i - 1, 0));
			if (ind === 0) {
				ctx.fillStyle = 'red';
			} else if (ind > -1) {
				ctx.fillStyle = 'blue';
			}
		}
		if (!discovery.intervals[ind + 1]) {
			ctx.fillStyle = 'black';
		}
		ctx.fillText(stringName, stringName.length === 1 ? 20 : 10, height + 10);
		ctx.fillStyle = 'black';
		ctx.fillRect(60, height, canvas.width - 90, 3);
	}
	// Draw frets and position markings
	// Start with initial bar
	ctx.font = '11pt Arial';
	ctx.fillRect(58, 20, 4, canvas.height - 30 * 2);
	for (var j = 1; j <= curInstrument.frets; j++) {
		var width = getFretWidth(j);
		var int = getFretWidthInt();
		ctx.fillRect(width, 20, 2, canvas.height - 30 * 2);
		// Write number of fret position
		if (j < 10) {
			ctx.fillText(j, width - int / 2 - 4, canvas.height - 22);
		} else {
			ctx.fillText(j, width - int / 2 - 7, canvas.height - 22);
		}
		// Draw making if it exists
		if (curInstrument.markings.indexOf(j) >= 0) {
			var cX = width - int / 2;
			if (j !== 12) {
				var cY = getStringHeight(Math.floor(curInstrument.numStrings / 2) - 1) + getStringHeightInt() / 2;
				if (curInstrument.numStrings % 2 === 1) {
					cY = getStringHeight(Math.floor(curInstrument.numStrings / 2));
				}
				ctx.beginPath();
				ctx.arc(cX, cY, 8, 0, 2 * Math.PI, false);
				ctx.fill();
			} else {
				var cY1 = getStringHeight(0) + getStringHeightInt() / 2;
				var cY2 = getStringHeight(curInstrument.numStrings - 1) - getStringHeightInt() / 2;
				ctx.beginPath();
				ctx.arc(cX, cY1, 8, 0, 2 * Math.PI, false);
				ctx.arc(cX, cY2, 8, 0, 2 * Math.PI, false);
				ctx.fill();
			}
		}
	}
	// Draw noteboxes
	if (!quiz.enabled) {
		if (curHoverNote) {
			drawNote(curHoverNote);
		}
		for (var k = 0; k < noteBoxes.length; k++) {
			var n = noteBoxes[k];
			if (n.active1 || n.active2 || n.active3) {
				drawNote(n);
			}
		}
	} else {
		drawNote(quiz.note);
	}
}
