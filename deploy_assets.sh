#!/bin/bash
# compile less into one css file
lessc assets/css/precompile/main.less assets/css/main.css
aws s3 sync --exclude "*.less" --exclude "precompile" ./assets s3://assets.weastie.com
