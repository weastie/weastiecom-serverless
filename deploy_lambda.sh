#!/bin/bash
cd ./code
node render.js
zip -r function.zip .
aws lambda update-function-code --function-name Weastiecom --zip-file fileb://function.zip
rm function.zip
