Welcome to the repository for the code that runs behind the serverless architecture
of [weastie.com](https://wwww.weastie.com). Here is a basic overview of the file structure.

---

* deploy_lambda.sh: zips code and deploys to lambda
* deploy_assets.sh: syncs assets folder with S3 bucket
* deploy.sh: runs deploy_lambda.sh and deploy_assets.sh
* assets: self explanatory folder of all the static files hosted on [assets.weastie.com](https://assets.weastie.com)
* code: all the code run on the lambda that powers weastie.com
	* index.js: main lambda script
	* render.js: compiles all of the pug files in templates.json
	* templates.json: listing of all pug files and their relative "short name", which is used in index.js
	* views: all of the pug files
		* compiled: folder to dump all the compiled pug files
