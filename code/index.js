var fs = require('fs');
var AWS = require('aws-sdk');
var got = require('got');
var jwt = require('jsonwebtoken');

AWS.config.update({region: 'us-east-1'});

var ddb = new AWS.DynamoDB.DocumentClient();

function genRes (body, options) {
	options = options || {};
	var out = {
		statusCode: 200,
		body: body,
		headers: {
			'Content-Type': 'text/html'
		}
	};
	for (var key in options.headers) {
		out.headers[key] = options.headers[key];
	}
	return out;
}

function redirect (url, statusCode) {
	statusCode = statusCode || 302;
	return {
		statusCode: statusCode,
		headers: {
			'Location': url
		}
	};
}

function renderTempl (name, options, resOptions) {
	eval(fs.readFileSync('views/compiled/' + name + '.js', 'utf8')); // eslint-disable-line
	options = options || {};
	if (!options.alerts) {
		options.alerts = [];
	}
	options.session = global.session;
	return genRes(template(options), resOptions); // eslint-disable-line
}

function getPathVars (path) {
	var s1 = path.split('/');
	var s2 = global.event.rawPath.split('/');
	var dict = {};
	for (var i = 0; i < s1.length; i++) {
		if (s1[i][0] === ':') {
			if (s2[i]) {
				dict[s1[i].substr(1)] = s2[i];
			}
		}
	}
	return dict;
}

function checkPath (paths, userPath) {
	if (userPath.length > 1 && userPath.substr(-1) === '/') {
		userPath = userPath.slice(0, -1);
	}
	var s1 = userPath.split('/');
	for (var i = 0; i < paths.length; i++) {
		var s2 = paths[i].split('/');
		var works = true;
		if (s1.length !== s2.length) {
			works = false;
		} else {
			for (var j = 0; j < s1.length; j++) {
				if (s2[j] === '*') {
					// ur good lol
				} else if (s2[j][0] === ':') {
					if (!s1[j]) {
						works = false;
					}
				} else if (s1[j] !== s2[j]) {
					works = false;
				}
			}
		}
		if (works) {
			global.params = getPathVars(paths[i]);
			return true;
		}
	}
	return false;
}

function getEventBody () {
	var body;
	if (global.event.isBase64Encoded) {
		body = Buffer.from(global.event.body, 'base64').toString('utf-8');
	} else {
		body = global.event.body;
	}
	if (global.event.headers['content-type'] === 'application/x-www-form-urlencoded') {
		var splitAnd = body.split('&');
		var dict = {};
		for (var i = 0; i < splitAnd.length; i++) {
			var splitEq = splitAnd[i].split('=');
			if (splitEq.length === 2) {
				dict[splitEq[0]] = decodeURIComponent(splitEq[1]).replace(/\+/g, ' ');
			}
		}
		return dict;
	} else if (global.event.headers['content-type'] === 'application/json') {
		return JSON.parse(body);
	} else {
		return {};
	}
}

function getSession (secret) {
	if (global.event.cookies) {
		for (var i = 0; i < global.event.cookies.length; i++) {
			var splitEq = global.event.cookies[i].split('=');
			if (splitEq[0] === 'session') {
				try {
					var decoded = jwt.verify(splitEq[1], secret, {algorithms: ['HS256']});
					return decoded;
				} catch (err) {
					return {};
				}
			}
		}
		return {};
	} else {
		return {};
	}
}

function setCookie (name, value, options) {
	return {
		'Set-Cookie': name + '=' + value + '; HttpOnly'
	};
}

function route (paths, method) {
	if (typeof paths === 'string') {
		paths = [paths];
	}
	if (checkPath(paths, global.event.rawPath) && (method === '*' || method.toLowerCase() === global.event.requestContext.http.method.toLowerCase())) {
		return true;
	}
	return false;
}

function genID (size) {
	var chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	var out = '';
	for (var i = 0; i < size; i++) {
		out += chars[Math.floor(Math.random() * chars.length)];
	}
	return out;
}

exports.handler = async (event, callback) => {
	global.event = event;
	var secret = JSON.parse(fs.readFileSync('secret.json', 'utf8'));
	var session = getSession(secret.jwt_secret);
	global.session = session;
	if (route('/', 'GET')) {
		return renderTempl('home');
	}
	if (route('/robots.txt', '*')) {
		return genRes('User-agent: *\nDisallow:\n', {
			headers: {
				'Content-Type': 'text/plain'
			}
		});
	}
	// blog
	if (route(['/blogs', '/blogs/:tag'], 'GET')) {
		let query = {
			TableName: 'blogs',
			ProjectionExpression: '#id, #title, #date, #tags',
			ExpressionAttributeNames: {
				'#id': 'id',
				'#title': 'title',
				'#date': 'date',
				'#tags': 'tags',
				'#deleted': 'deleted'
			},
			ExpressionAttributeValues: {
				':false': false
			},
			FilterExpression: '#deleted = :false'
		};
		if (global.params.tag) {
			query.FilterExpression += ' and contains(#tags, :tag)';
			query.ExpressionAttributeValues[':tag'] = global.params.tag;
		}
		let out = await ddb.scan(query).promise();
		out.Items.sort(function (a, b) {
			return b.date - a.date;
		});
		return renderTempl('view_blogs', {blogs: out.Items, filter: global.params.tag});
	}
	if (route('/blog/:id/*', 'GET')) {
		let query = {
			TableName: 'blogs',
			Key: {
				id: global.params.id
			}
		};
		let out = await ddb.get(query).promise();
		if (out.Item) {
			return renderTempl('view_blog', {blog: out.Item});
		} else {
			return renderTempl('result', {error: true, text: 'Blog not found'});
		}
	}
	// projects
	if (route('/projects', 'GET')) {
		return renderTempl('projects');
	}
	// music tools
	if (route('/music_tools', 'GET')) {
		return renderTempl('music_tools_home');
	}
	if (route('/music_tools/fretboard', 'GET')) {
		return renderTempl('music_tools_fretboard');
	}
	// games
	if (route('/games/chess', 'GET')) {
		return renderTempl('game_chess');
	}
	// hack tools
	if (route('/hack_tools/listener', 'GET')) {
		return renderTempl('hack_tools_listener_setup');
	}
	if (route('/hack_tools', 'GET')) {
		return renderTempl('hack_tools_home');
	}
	if (route('/hack_tools/converter', 'GET')) {
		return renderTempl('hack_tools_converter');
	}
	if (route('/hack_tools/create_listener', 'POST')) {
		// check google key
		let body = getEventBody();
		const verify = await got.post('https://www.google.com/recaptcha/api/siteverify', {
			form: {
				secret: secret.captcha_secret,
				response: body['g-recaptcha-response'],
				remoteip: event.requestContext.http.sourceIp
			}
		}).json();
		if (verify.success) {
			// create listener in dynamodb!
			var id = genID(4);
			let query = {
				TableName: 'listeners',
				Item: {
					id: id,
					created: Date.now(),
					requests: [],
					expires: Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 2, // We divide Date.now() by 1000 to get seconds rather than ms
					size: 0
				}
			};
			await ddb.put(query).promise();
			return redirect('/hack_tools/listener/' + id);
		} else {
			return renderTempl('hack_tools/listener/setup', {alerts: [{message: 'Please complete captcha', type: 'error'}]});
		}
	};
	if (route('/hack_tools/listener/:id', 'GET')) {
		let query = {
			TableName: 'listeners',
			Key: {
				id: global.params.id
			}
		};
		let out = await ddb.get(query).promise();
		if (out.Item) {
			out.Item.requests.sort(function (a, b) {
				return b.date - a.date;
			});
			return renderTempl('hack_tools_listener', {data: out.Item, id: global.params.id});
		} else {
			return renderTempl('result', {error: true, text: 'Listener not found'});
		}
	}
	if (route(['/l/:id', '/l/:id/*'], '*')) {
		let body = getEventBody();
		let newEntry = {
			date: Date.now(),
			method: event.requestContext.http.method,
			ip: event.requestContext.http.sourceIp,
			path: event.rawPath,
			query: event.rawQueryString || 'none',
			headers: event.headers,
			body: body
		};
		// Rough estimate of size calculation
		let size = JSON.stringify(body).length + event.rawPath.length +
							event.rawQueryString.length + JSON.stringify(event.headers).length +
							event.requestContext.http.method.length + event.requestContext.http.sourceIp.length;
		// first, check that the table has enough size left
		let query1 = {
			TableName: 'listeners',
			Key: {
				id: global.params.id
			},
			ProjectionExpression: '#size',
			ExpressionAttributeNames: {
				'#size': 'size'
			}
		};
		let out = await ddb.get(query1).promise();
		if (out.Item && (out.Item.size + size <= 75000)) {
			// Listener exists and is not full, add more data
			let query2 = {
				TableName: 'listeners',
				Key: {
					id: global.params.id
				},
				UpdateExpression: 'SET #requests = list_append(#requests, :newEntry), #size = #size + :size',
				ExpressionAttributeNames: {
					'#requests': 'requests',
					'#size': 'size'
				},
				ExpressionAttributeValues: {
					':newEntry': [newEntry],
					':size': size
				}
			};
			await ddb.update(query2).promise();
			return redirect('/');
		} else {
			// data can not be entered or table doesn't exist, just redirect
			return redirect('/');
		}
	}
	if (route('/hack_tools/html', 'GET')) {
		return renderTempl('hack_tools_html');
	}
	if (route('/hack_tools/create_html', 'POST')) {
		let body = getEventBody();
		// First, check that the body is small enough
		if (body.content.length >= 50000) {
			return renderTempl('hack_tools_html', {alerts: [{message: 'You must upload less than 50kb of data', type: 'error'}]});
		}
		// Verify google captcha
		let secret = JSON.parse(fs.readFileSync('secret.json', 'utf8'));
		const verify = await got.post('https://www.google.com/recaptcha/api/siteverify', {
			form: {
				secret: secret.captcha_secret,
				response: body['g-recaptcha-response'],
				remoteip: event.requestContext.http.sourceIp
			}
		}).json();
		if (verify.success) {
			// Upload to database
			let id = genID(4);
			let query = {
				TableName: 'html_pages',
				Item: {
					id: id,
					created: Date.now(),
					content: body.content,
					expires: Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 2 // We divide Date.now() by 1000 to get seconds rather than ms
				}
			};
			await ddb.put(query).promise();
			return redirect('/hack_tools/html_page/' + id);
		} else {
			return renderTempl('hack_tools_html', {alerts: [{message: 'Please complete captcha', type: 'error'}]});
		}
	}
	if (route('/hack_tools/html_page/:id', '*')) {
		let query = {
			TableName: 'html_pages',
			Key: {
				id: global.params.id
			}
		};
		let out = await ddb.get(query).promise();
		if (out.Item) {
			return genRes(decodeURIComponent(out.Item.content));
		} else {
			return renderTempl('result', {error: true, text: 'HTML page not found'});
		}
	}
	// admin
	if (route('/admin', 'GET')) {
		if (session.admin) {
			return renderTempl('admin_home');
		} else {
			return renderTempl('admin_login');
		}
	}
	if (route('/admin', 'POST')) {
		let body = getEventBody();
		if (body.password === secret.admin_pass) {
			let cookie = jwt.sign({admin: true}, secret.jwt_secret, {expiresIn: '2 days'});
			return renderTempl('admin_home', {}, {headers: setCookie('session', cookie)});
		} else {
			return renderTempl('admin_login', {fail: true});
		}
	}
	if (route('/admin/deleted_blogs', 'GET')) {
		if (session.admin) {
			let query = {
				TableName: 'blogs',
				ProjectionExpression: '#id, #title, #date, #tags',
				ExpressionAttributeNames: {
					'#id': 'id',
					'#title': 'title',
					'#date': 'date',
					'#tags': 'tags',
					'#deleted': 'deleted'
				},
				ExpressionAttributeValues: {
					':true': true
				},
				FilterExpression: '#deleted = :true'
			};
			let out = await ddb.scan(query).promise();
			out.Items.sort(function (a, b) {
				return b.date - a.date;
			});
			return renderTempl('view_blogs', {blogs: out.Items, filter: ''});
		} else {
			return redirect('/admin');
		}
	}
	if (route('/admin/create_blog', 'GET')) {
		if (session.admin) {
			return renderTempl('admin_create_blog');
		} else {
			return redirect('/admin');
		}
	}
	if (route('/admin/create_blog', 'POST')) {
		if (session.admin) {
			let body = getEventBody();
			let id = genID(5);
			console.log(body);
			let query = {
				TableName: 'blogs',
				Item: {
					id: id,
					title: body.title,
					content: body.content,
					tags: body.tags,
					date: Date.now(),
					deleted: body.draft === 'on'
				}
			};
			await ddb.put(query).promise();
			return redirect('/blog/' + id + '/' + global.stringToPageTitle(body.title));
		} else {
			return redirect('/admin');
		}
	}
	if (route('/admin/edit_blog/:id', 'GET')) {
		if (session.admin) {
			// get blog
			let query = {
				TableName: 'blogs',
				Key: {
					id: global.params.id
				}
			};
			let out = await ddb.get(query).promise();
			if (out.Item) {
				return renderTempl('admin_edit_blog', {blog: out.Item});
			} else {
				return renderTempl('result', {error: true, text: 'Blog not found'});
			}
		} else {
			return redirect('/admin');
		}
	}
	if (route('/admin/edit_blog/:id', 'POST')) {
		if (session.admin) {
			let body = getEventBody();
			let query = {
				TableName: 'blogs',
				Key: {
					id: global.params.id
				},
				UpdateExpression: 'SET #tags = :tags, #content = :content, #title = :title, #deleted = :deleted',
				ExpressionAttributeNames: {
					'#tags': 'tags',
					'#content': 'content',
					'#title': 'title',
					'#deleted': 'deleted'
				},
				ExpressionAttributeValues: {
					':tags': body.tags,
					':content': body.content,
					':title': body.title,
					':deleted': body.deleted === 'on'
				}
			};
			if (body.updateTime === 'on') {
				query.UpdateExpression += ', #date = :date';
				query.ExpressionAttributeValues[':date'] = Date.now();
				query.ExpressionAttributeNames['#date'] = 'date';
			}
			await ddb.update(query).promise();
			return redirect('/blog/' + global.params.id + '/' + global.stringToPageTitle(body.title));
		} else {
			return redirect('/admin');
		}
	}
	if (route('/admin/delete_blog/:id', 'GET')) {
		if (session.admin) {
			let query = {
				TableName: 'blogs',
				Key: {
					id: global.params.id
				},
				UpdateExpression: 'SET #deleted = :deleted',
				ExpressionAttributeNames: {
					'#deleted': 'deleted'
				},
				ExpressionAttributeValues: {
					':deleted': true
				}
			};
			await ddb.update(query).promise();
			return redirect('/blogs');
		} else {
			return redirect('/admin');
		}
	}
	// 404
	return renderTempl('result', {error: true, text: '404 page not found'});
};

// global functions for pug
global.formatTitle = function (words) {
	// If the input is a single word, make it an array of itself for ease.
	if (typeof words === 'string') {
		words = [words];
	}
	words.reverse();
	var result = '';
	for (var i in words) {
		result += words[i] + ' | ';
	}
	result += 'Weastie';
	return result;
};

global.stringToPageTitle = function (string, length) {
	length = length || 45;
	// Replace white space with _ and ? # " ' ; with nothing
	return encodeURIComponent(string.substr(0, length).replace(/\s/g, '_').replace(/[(?)(#)(")(')(;)]/g, ''));
};
