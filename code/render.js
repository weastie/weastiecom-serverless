var fs = require('fs');
var pug = require('pug');

var templates = JSON.parse(fs.readFileSync('templates.json'));

for (var key in templates.templates) {
	var x = pug.compileFileClient(templates.templates[key], {basedir: templates.basedir});
	fs.writeFileSync('views/compiled/' + key + '.js', x.toString());
}
