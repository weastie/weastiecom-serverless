function pug_attr(t,e,n,r){if(!1===e||null==e||!e&&("class"===t||"style"===t))return"";if(!0===e)return" "+(r?t:t+'="'+t+'"');var f=typeof e;return"object"!==f&&"function"!==f||"function"!=typeof e.toJSON||(e=e.toJSON()),"string"==typeof e||(e=JSON.stringify(e),n||-1===e.indexOf('"'))?(n&&(e=pug_escape(e))," "+t+'="'+e+'"'):" "+t+"='"+e.replace(/'/g,"&#39;")+"'"}
function pug_classes(s,r){return Array.isArray(s)?pug_classes_array(s,r):s&&"object"==typeof s?pug_classes_object(s):s||""}
function pug_classes_array(r,a){for(var s,e="",u="",c=Array.isArray(a),g=0;g<r.length;g++)(s=pug_classes(r[g]))&&(c&&a[g]&&(s=pug_escape(s)),e=e+u+s,u=" ");return e}
function pug_classes_object(r){var a="",n="";for(var o in r)o&&r[o]&&pug_has_own_property.call(r,o)&&(a=a+n+o,n=" ");return a}
function pug_escape(e){var a=""+e,t=pug_match_html.exec(a);if(!t)return e;var r,c,n,s="";for(r=t.index,c=0;r<a.length;r++){switch(a.charCodeAt(r)){case 34:n="&quot;";break;case 38:n="&amp;";break;case 60:n="&lt;";break;case 62:n="&gt;";break;default:continue}c!==r&&(s+=a.substring(c,r)),c=r+1,s+=n}return c!==r?s+a.substring(c,r):s}
var pug_has_own_property=Object.prototype.hasOwnProperty;
var pug_match_html=/["&<>]/;
function pug_rethrow(n,e,t,r){if(!(n instanceof Error))throw n;if(!("undefined"==typeof window&&e||r))throw n.message+=" on line "+t,n;try{r=r||require("fs").readFileSync(e,"utf8")}catch(e){pug_rethrow(n,null,t)}var a=3,i=r.split("\n"),o=Math.max(t-a,0),h=Math.min(i.length,t+a),a=i.slice(o,h).map(function(n,e){var r=e+o+1;return(r==t?"  > ":"    ")+r+"| "+n}).join("\n");n.path=e;try{n.message=(e||"Pug")+":"+t+"\n"+a+"\n\n"+n.message}catch(n){}throw n}function template(locals) {var pug_html = "", pug_mixins = {}, pug_interp;var pug_debug_filename, pug_debug_line;try {;
    var locals_for_with = (locals || {});
    
    (function (JSON, alerts, blog, global, session, tags) {
      ;pug_debug_line = 1;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003C!DOCTYPE html\u003E";
;pug_debug_line = 3;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Chtml\u003E";
;pug_debug_line = 4;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Chead\u003E";
;pug_debug_line = 5;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cmeta name=\"viewport\" content=\"width=device-width,initial-scale=1\"\u003E";
;pug_debug_line = 6;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cmeta charset=\"UTF-8\"\u003E";
;pug_debug_line = 7;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cmeta name=\"author\" content=\"Weastie\"\u003E";
;pug_debug_line = 8;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Clink rel=\"icon\" href=\"https:\u002F\u002Fassets.weastie.com\u002Fimg\u002Ffavicon.ico\" type=\"image\u002Fx-icon\"\u003E";
;pug_debug_line = 9;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cscript src=\"https:\u002F\u002Fcode.jquery.com\u002Fjquery-3.5.1.min.js\" integrity=\"sha256-9\u002FaliU8dGd2tb6OSsuzixeV4y\u002FfaTqgFtohetphbbj0=\" crossorigin=\"anonymous\"\u003E\u003C\u002Fscript\u003E";
;pug_debug_line = 12;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Clink rel=\"stylesheet\" href=\"https:\u002F\u002Fassets.weastie.com\u002Fcss\u002Fmain.css\"\u003E";
;pug_debug_line = 13;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cscript src=\"https:\u002F\u002Fassets.weastie.com\u002Fscripts\u002Fbody.js\"\u003E\u003C\u002Fscript\u003E";
;pug_debug_line = 14;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
;pug_debug_line = 4;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + "\u003Ctitle\u003E";
;pug_debug_line = 4;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = global.formatTitle(['Blog', blog.title])) ? "" : pug_interp)) + "\u003C\u002Ftitle\u003E";
;pug_debug_line = 5;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + "\u003Clink rel=\"stylesheet\" type=\"text\u002Fcss\" href=\"https:\u002F\u002Fassets.weastie.com\u002Fcss\u002Fblog.css\"\u003E\u003C\u002Fhead\u003E";
;pug_debug_line = 15;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cbody\u003E";
;pug_debug_line = 17;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv class=\"drawer hidden\" id=\"mainDrawer\"\u003E";
;pug_debug_line = 18;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cul\u003E";
;pug_debug_line = 19;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 20;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Ca href=\"\u002Fblogs\"\u003E";
;pug_debug_line = 20;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "Blog\u003C\u002Fa\u003E\u003C\u002Fli\u003E";
;pug_debug_line = 21;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 22;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Ca href=\"\u002Fprojects\"\u003E";
;pug_debug_line = 22;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "Projects\u003C\u002Fa\u003E\u003C\u002Fli\u003E\u003C\u002Ful\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 23;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv id=\"body\"\u003E";
;pug_debug_line = 24;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv class=\"navBar\" id=\"mainNavBar\"\u003E";
;pug_debug_line = 25;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv class=\"container\"\u003E";
;pug_debug_line = 26;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv\u003E";
;pug_debug_line = 27;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cul\u003E";
;pug_debug_line = 28;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 29;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Ca class=\"drawerBtn\" href=\"#\" data-for=\"mainDrawer\"\u003E";
;pug_debug_line = 30;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cspan class=\"drawerBtnBar\"\u003E\u003C\u002Fspan\u003E";
;pug_debug_line = 31;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cspan class=\"drawerBtnBar\"\u003E\u003C\u002Fspan\u003E";
;pug_debug_line = 32;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cspan class=\"drawerBtnBar\"\u003E\u003C\u002Fspan\u003E\u003C\u002Fa\u003E\u003C\u002Fli\u003E";
;pug_debug_line = 33;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 34;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Ca href=\"\u002F\"\u003E";
;pug_debug_line = 35;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cspan\u003E";
;pug_debug_line = 35;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "Home\u003C\u002Fspan\u003E\u003C\u002Fa\u003E\u003C\u002Fli\u003E\u003C\u002Ful\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 36;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv class=\"bigOnly\"\u003E";
;pug_debug_line = 37;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cul\u003E";
;pug_debug_line = 38;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 39;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Ca href=\"\u002Fblogs\"\u003E";
;pug_debug_line = 39;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "Blog\u003C\u002Fa\u003E\u003C\u002Fli\u003E";
;pug_debug_line = 40;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 41;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Ca href=\"\u002Fprojects\"\u003E";
;pug_debug_line = 41;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "Projects\u003C\u002Fa\u003E\u003C\u002Fli\u003E\u003C\u002Ful\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 43;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv class=\"container contentBody\"\u003E";
;pug_debug_line = 44;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cspan" + (" class=\"hidden\""+pug_attr("data-alerts", (JSON.stringify(alerts)), true, true)+" id=\"serverAlerts\"") + "\u003E\u003C\u002Fspan\u003E";
;pug_debug_line = 45;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv id=\"alertsDiv\"\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 46;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
;pug_debug_line = 8;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
if (session.admin) {
;pug_debug_line = 9;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + "\u003Cform" + (" method=\"GET\""+pug_attr("action", ('/admin/delete_blog/' + blog.id), true, true)+" onsubmit=\"return confirm(&quot;Are you sure?&quot;)\"") + "\u003E";
;pug_debug_line = 10;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + "\u003Cinput class=\"btn btnRaised btnRed\" type=\"submit\" value=\"Delete\"\u003E\u003C\u002Fform\u003E";
;pug_debug_line = 11;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + "\u003Ca" + (pug_attr("href", ('/admin/edit_blog/' + blog.id), true, true)) + "\u003E";
;pug_debug_line = 12;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + "\u003Cinput class=\"btn btnRaised btnBlue\" type=\"button\" value=\"Edit\"\u003E\u003C\u002Fa\u003E";
}
;pug_debug_line = 13;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + "\u003Ch1\u003E";
;pug_debug_line = 13;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + (null == (pug_interp = blog.title) ? "" : pug_interp) + "\u003C\u002Fh1\u003E";
;pug_debug_line = 14;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
tags = blog.tags ? blog.tags.split(',') : []
;pug_debug_line = 15;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
// iterate tags
;(function(){
  var $$obj = tags;
  if ('number' == typeof $$obj.length) {
      for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
        var tag = $$obj[pug_index0];
;pug_debug_line = 16;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + "\u003Ca" + (pug_attr("class", pug_classes([('tag ' + 'tag-' + tag),"btnRaised"], [true,false]), false, true)+pug_attr("href", ('/blogs/' + tag), true, true)) + "\u003E";
;pug_debug_line = 16;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = tag) ? "" : pug_interp)) + "\u003C\u002Fa\u003E";
      }
  } else {
    var $$l = 0;
    for (var pug_index0 in $$obj) {
      $$l++;
      var tag = $$obj[pug_index0];
;pug_debug_line = 16;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + "\u003Ca" + (pug_attr("class", pug_classes([('tag ' + 'tag-' + tag),"btnRaised"], [true,false]), false, true)+pug_attr("href", ('/blogs/' + tag), true, true)) + "\u003E";
;pug_debug_line = 16;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = tag) ? "" : pug_interp)) + "\u003C\u002Fa\u003E";
    }
  }
}).call(this);

;pug_debug_line = 17;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + "\u003Cp class=\"dateGrey formatDate\"\u003E";
;pug_debug_line = 17;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + (null == (pug_interp = blog.date) ? "" : pug_interp) + "\u003C\u002Fp\u003E";
;pug_debug_line = 18;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + "\u003Chr\u003E";
;pug_debug_line = 19;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + "\u003Cdiv\u003E";
;pug_debug_line = 19;pug_debug_filename = "views\u002Fgeneral\u002Fview_blog.pug";
pug_html = pug_html + (null == (pug_interp = blog.content) ? "" : pug_interp) + "\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fbody\u003E\u003C\u002Fhtml\u003E";
    }.call(this, "JSON" in locals_for_with ?
        locals_for_with.JSON :
        typeof JSON !== 'undefined' ? JSON : undefined, "alerts" in locals_for_with ?
        locals_for_with.alerts :
        typeof alerts !== 'undefined' ? alerts : undefined, "blog" in locals_for_with ?
        locals_for_with.blog :
        typeof blog !== 'undefined' ? blog : undefined, "global" in locals_for_with ?
        locals_for_with.global :
        typeof global !== 'undefined' ? global : undefined, "session" in locals_for_with ?
        locals_for_with.session :
        typeof session !== 'undefined' ? session : undefined, "tags" in locals_for_with ?
        locals_for_with.tags :
        typeof tags !== 'undefined' ? tags : undefined));
    ;} catch (err) {pug_rethrow(err, pug_debug_filename, pug_debug_line);};return pug_html;}